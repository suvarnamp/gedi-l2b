#!/usr/bin/env/python3
'''
Description: A collection of Python 3 functions to process GEDI Level 2B data.
Authors: Suvarna Punalekar & Osian Roberts (Aberystwyth University).
'''
import os
import h5py
import pandas as pd
from sys import stdout


class read_l2b:
    '''
    Methods to process GEDI Level 2B data.
    '''
    def __init__(self, infile, xmin, xmax, ymin, ymax, verbose=True):
        '''
        A function to read GEDI Level 2B data from a HDF-5 file.

        Required parameters:
        infile = Input file name/path.
        xmin, xmax, ymin, ymax = Coordinates of bounding box to subset data to
                                 ROI.
        '''
        if not isinstance(infile, str):
            raise SystemExit('Error: the input file name must be a string.')
        if not os.path.exists(infile):
            raise SystemExit('Error: input file not found.')

        try:
            self.f = h5py.File(infile, 'r')
        except Exception:
            raise SystemExit('Error: unable to open the input file.')

        # read metadata:
        self.read_metadata(verbose)

        # read metrics:
        self.read_metrics(xmin, xmax, ymin, ymax, verbose)

        # close the HDF-5 file:
        self.f.close()


    def read_metadata(self, verbose=True):
        ''' A function to read GEDI Level 2B metadata. '''
        try:
            self.metadata = dict()
            for key, value in self.f.get('/METADATA/DatasetIdentification').attrs.items():
                if key == 'fileName':
                    self.metadata['fileName'] = value
                    value = value.split('_')[2]
                    self.metadata['DOY'] = '-'.join([value[4:6], value[6:8], value[:4]])
                    self.metadata['Year'] = int(value[:4])
                    self.metadata['Month'] = int(value[6:8])
                    self.metadata['Day'] = int(value[4:6])
                elif key == 'shortName':
                    self.metadata['shortName'] = value
                elif key == 'VersionID':
                    self.metadata['VersionID'] = value
                elif key == 'spatialRepresentationType':
                    self.metadata['spatialRepresentation'] = value
                else:
                    continue

            # check that dataset is Level 2B:
            if self.metadata['shortName'] != 'GEDI_L2B':
                raise SystemExit('Error: input dataset is not GEDI Level 2B.')

        except Exception:
            if verbose is True:
                print('WARNING: unable to read GEDI Level 2B metadata.')


    def read_metrics(self, xmin, xmax, ymin, ymax, verbose=True):
        '''
        A function to read GEDI metrics.

        Required parameters:
        xmin, xmax, ymin, ymax = Coordinates of bounding box to subset data to
                                 ROI.

        Optional parameters:
        verbose = Boolean option to enable/disable printing to the console.
        '''
        # use default verbose value if input not boolean:
        if not isinstance(verbose, bool):
            verbose = True

        if verbose is True:
            print('Reading lidar metrics from ' + self.metadata['fileName'])

        BeamNo = [name for name in list(self.f) if name != 'METADATA']

        colnames = ['BeamNo', 'DOY', 'lat_highestreturn', 'lat_lowestmode',
                    'latitude_bin0', 'latitude_lastbin', 'latitude_bin0_error',
                    'latitude_lastbin_error', 'lon_highestreturn', 'lon_lowestmode',
                    'longitude_bin0', 'longitude_lastbin', 'longitude_bin0_error',
                    'longitude_lastbin_error', 'cover', 'cover_z', 'pai', 'pai_z',
                    'pavd_z', 'pgap_theta', 'pgap_theta_error', 'fhd_normal']

        df = pd.DataFrame(columns=colnames)

        for Beam in sorted(BeamNo):
            if verbose is True:
                print('Processing ' + Beam + '...')

            df_subset = pd.DataFrame(columns=colnames)
            geolocpath = Beam + '/geolocation'

            if geolocpath in self.f:
                for j1 in range(2, 14):
                    col = colnames[j1]
                    array = self.f.get(Beam + '/geolocation/' + col)[()]
                    df_subset[col] = array
                    del col, array

                for j2 in range(14, len(colnames)):
                    col = colnames[j2]
                    array = self.f.get(Beam + '/' + col)[()]
                    df_subset[col] = array
                    del col, array

                df_subset['BeamNo'] = Beam
                df_subset['DOY'] = self.metadata['DOY']

                # subset data to ROI:
                try:
                    df_subset = df_subset[(df_subset['lat_highestreturn'] >= ymin) &
                                          (df_subset['lat_highestreturn'] <= ymax) &
                                          (df_subset['lon_highestreturn'] >= xmin) &
                                          (df_subset['lon_highestreturn'] <= xmax) &
                                          (df_subset['cover'] != -9999)]
                    df = df.append(df_subset)
                except Exception:
                    df = df.append(df_subset)
                    if verbose is True:
                        print('WARNING: Unable to subset data to ROI for beam ' + Beam)

            else:
                del df_subset
                if verbose is True:
                    print('WARNING: No geolocation info for ' + Beam)

            del df_subset, geolocpath

        if df.empty is False:
            df.reset_index(drop=True, inplace=True)

        self.data = df
        del df


    def export_to_CSV(self, outFile, sep=',', header=True, index=False):
        '''
        Description: A function to export GEDI data to a text file.

        Required parameters:
        outFile = Output file name/path.

        Optional parameters:
        sep = column separator/delimiter. Default=comma.
        header = boolean option to write header.
        index = boolean option to write row index.
        '''
        if self.data.empty is True:
            print('Error: no valid data to export to CSV.')
        else:
            print('Exporting data to CSV file...')
            self.data.to_csv(outFile, sep=sep, header=header, index=index)
            print('Done.')


    def export_to_HDF(self, outfile, hdfpath='/DATA'):
        '''
        Description: A function to export GEDI data to a HDF-5 file.

        Required parameters:
        outFile = Output file name/path.

        Optional parameters:
        hdfpath = destination directory in HDF-5 file. Default = /DATA.
        '''
        if self.data.empty is True:
            print('Error: no valid data to export to HDF.')
        else:
            print('Exporting data to HDF-5 file...')
            import numpy as np
            try:
                import h5py
            except Exception:
                print('Error: unable to import h5py.')
                return

            # get colnames:
            colnames = list(self.data)

            # define datatypes:
            dtypes = []
            for name in colnames:
                if self.data[name].dtype == 'object':
                    dtypes.append('a10')
                else:
                    dtypes.append('f8')

            dtypes = ','.join(dtypes)

            # convert pandas dataframe to np.recarray:
            data = self.data.values.T
            data = np.core.records.fromarrays(data, names=colnames,
                                              formats=dtypes)
            del colnames, dtypes

            # write array to file:
            if os.path.exists(outfile):
                hdf = h5py.File(outfile, 'a')
                if hdfpath in hdf:
                    del hdf[hdfpath]
                hdf.create_dataset(hdfpath, data=data, compression='gzip',
                                   compression_opts=1)
            else:
                hdf = h5py.File(outfile, 'w')
                hdf.create_dataset(hdfpath, data=data, compression='gzip',
                                   compression_opts=1)
            del data
            print('Done.')


    def export_to_vector(self, outFile, outFormat='GPKG', diameter=25,
                         verbose=True):
        '''
        Description: A function to export GEDI data to a vector dataset.

        Required parameters:
        outfile = String defining the output file name.

        Optional parameters:
        outformat = Output vector format. Default = 'GPKG'.
        diameter = laser footprint diameter in metres.
        verbose = Boolean option to enable/disable printing to the console.
        '''
        import math
        from osgeo import ogr, osr

        if not isinstance(verbose, bool):
            verbose = True

        if verbose is True:
            print('Exporting data to a vector file...')

        # check that data is not empty:
        if self.data.empty is True:
            if verbose is True:
                print('Error: no valid data to export.')
            return

        if not isinstance(outFile, str):
            raise SystemExit('Error: the output file path must be a string.')
        if not isinstance(outFormat, str):
            outformat = 'GPKG'

        # get vector driver:
        try:
            driver = ogr.GetDriverByName(outFormat)
        except Exception:
            raise SystemExit('Error: vector format not recognised.')

        # delete existing vector if present:
        if os.path.exists(outFile):
            driver.DeleteDataSource(outFile)

        # create vector dataset:
        vector = driver.CreateDataSource(outFile)

        # define coordinate system:
        sourceSRS = osr.SpatialReference()
        sourceSRS.ImportFromEPSG(4326)

        # define vector layer:
        layerName = os.path.basename(outFile).split('.')[0]
        layer = vector.CreateLayer(layerName, sourceSRS,
                                   geom_type=ogr.wkbMultiPolygon)
        del layerName

        # define vector fields:
        fields = list(self.data)

        for name in fields:
            if self.data[name].dtype == 'float32' or self.data[name].dtype == 'float64':
                field = ogr.FieldDefn(name, ogr.OFTReal)
                field.SetWidth(10)
                field.SetPrecision(7)
                layer.CreateField(field)
            else:
                field = ogr.FieldDefn(name, ogr.OFTString)
                field.SetWidth(10)
                layer.CreateField(field)
            del field

        # iterate over each pulse in ROI
        for idx, pulse in self.data.iterrows():
            # create point at centre of footprint:
            point = ogr.Geometry(ogr.wkbPoint)
            point.AddPoint(float(pulse['lon_highestreturn']),
                           float(pulse['lat_highestreturn']))

            # convert from WGS84 to local Geographic coordinate system
            utm_band = str(int(math.floor((pulse['lon_highestreturn'] + 180) /
                               6) % 60) + 1).zfill(2)
            if pulse['lat_highestreturn'] >= 0:
                EPSG = int('326' + utm_band)
            else:
                EPSG = int('327' + utm_band)

            geogSRS = osr.SpatialReference()
            geogSRS.ImportFromEPSG(EPSG)
            transform = osr.CoordinateTransformation(sourceSRS, geogSRS)
            point.Transform(transform)
            point.FlattenTo2D()
            del transform, utm_band, EPSG

            # buffer point by the 25 m footprint diameter
            poly = point.Buffer(diameter/2)
            del point

            # convert geometry into WGS84:
            transform = osr.CoordinateTransformation(geogSRS, sourceSRS)
            poly.Transform(transform)
            del transform

            # create feature in vector layer:
            feature = ogr.Feature(layer.GetLayerDefn())
            feature.SetGeometry(poly)
            del poly

            # populate fields:
            for name in fields:
                if self.data[name].dtype == 'float32' or self.data[name].dtype == 'float64':
                    feature.SetField(name, float(pulse[name]))
                else:
                    feature.SetField(name, str(pulse[name]))

            # write feature to vector:
            layer.CreateFeature(feature)
            layer.SyncToDisk()
            del feature

            if verbose is True:
                ProgressBar(self.data.shape[0], idx + 1)

        # close datasets:
        del vector, layer, fields


def ProgressBar(n_tasks, progress):
    ''' A function to display a progress bar on the unix terminal.'''
    barLength, status = 50, ''
    progress = float(progress) / float(n_tasks)
    if progress >= 1.0:
        progress = 1
        status = 'Done. \n'
    block = round(barLength * progress)
    text = '\r{} {:.0f}% {}'.format('#' * block + '-' * (barLength - block),
                                    round(progress * 100, 0), status)
    stdout.write(text)
    stdout.flush()


def batch_processor(idx, filelist, outdir, xmin, xmax, ymin, ymax, diameter=25,
                    vector=True, csv=False, sep=',', header=True, index=False,
                    hdf=False, hdfpath='/DATA', verbose=False):
    '''
    An utility function to process GEDI data with multiple CPU cores.

    Required Parameters:
    idx = Integer denoting index used to select the input file from filelist.
    filelist = List of input files.
    outdir = String defining path to the output directory. The path will be
    created if it does not already exist.
    xmin, xmax, ymin, ymax = Coordinates of bounding box to subset data to ROI.

    Optional parameters:
    diameter = laser footprint diameter in metres.
    vector = Boolean option to enable/disable exporting data to vector.
    csv = Boolean option to enable/disable exporting data to text file.
    sep = Column separator for csv file.
    header = Boolean option to enable/disable writing column headers in csv.
    index = Boolean option to enable/disable writing row index in csv file.
    hdf = Boolean option to enable/disable exporting data to a HDF-5 file.
    hdfpath = destination directory in HDF-5 file. Default = /DATA.
    verbose = Boolean option to enable/disable printing to the console.
              Default = False.
    '''
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # select input file using multiprocessing iterator:
    inFile = filelist[idx]

    print('Processing', os.path.basename(inFile))
    try:
        # read GEDI Level 2B data in ROI:
        ds = read_l2b(inFile, xmin, xmax, ymin, ymax, verbose)

        if vector is True:
            # export metrics to vector:
            outfile = outdir + os.path.basename(inFile).replace('.h5', '.gpkg')
            ds.export_to_vector(outfile, verbose=verbose)

        if csv is True:
            # export metrics to csv file:
            outfile = outdir + os.path.basename(inFile).replace('.h5', '.csv')
            ds.export_to_CSV(outfile, sep, header, index)

        if hdf is True:
            # export metrics to HDF-5 file:
            outfile = outdir + os.path.basename(inFile)
            ds.export_to_HDF(outfile, hdfpath)

        # delete object & pointer:
        del ds
    except Exception:
        print('Error: could not process file.')


def apply_batch_processor(indir, outdir, xmin, xmax, ymin, ymax, ncores=4,
                          searchString='/**/GEDI02_B*.h5', diameter=25,
                          vector=True, csv=False, sep=',', header=True,
                          index=False, hdf=False, hdfpath='/DATA',
                          verbose=False):
    '''
    Description: A function to process a list of HDF-5 files containing IceSAT2
    data using multi-core processing.

    Required parameters:
    indir = Input directory in which to recursively search for ATL08 files.
    outdir = String defining path to the output directory. The path will be
    created if it does not already exist.
    xmin, xmax, ymin, ymax = Coordinates of bounding box to subset data to ROI.

    Optional parameters:
    ncores = Integer defining the number of CPU threads to use. Default = 4.
    searchString = Search string used to find files.
    diameter = laser footprint diameter in metres.
    vector = Boolean option to enable/disable exporting data to vector.
    csv = Boolean option to enable/disable exporting data to text file.
    sep = Column separator for csv file.
    header = Boolean option to enable/disable writing column headers in csv.
    index = Boolean option to enable/disable writing row index in csv file.
    hdf = Boolean option to enable/disable exporting data to a HDF-5 file.
    hdfpath = destination directory in HDF-5 file. Default = /DATA.
    verbose = Boolean option to enable/disable printing to the console.
              Default = False.
    '''
    # set values to defaults if invalid inputs are provided:
    if not isinstance(verbose, bool):
        verbose = False
    if not isinstance(ncores, int):
        ncores = 4
    if not isinstance(searchString, str):
        searchString = '/**/GEDI02_B*.h5'
    # check input directory exists:
    if not os.path.exists(indir):
        raise SystemExit('Error: input directory does not exist.')
    else:
        indir = os.path.abspath(indir) + '/'

    # seach for HDF-5 files in input directory:
    import glob
    filelist = sorted(glob.glob(indir + searchString, recursive=True))
    if not filelist:
        raise SystemExit('Error: no HDF-5 files were found.')

    # create the output directory:
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    outdir = os.path.abspath(outdir) + '/'

    nFiles = len(filelist)
    if nFiles == 1:
        # process single file without multi-threading:
        batch_processor(0, filelist=filelist, outdir=outdir, xmin=xmin,
                        xmax=xmax, ymin=ymin, ymax=ymax, diameter=diameter,
                        vector=vector, csv=csv, sep=sep, header=header,
                        index=index, hdf=hdf, hdfpath=hdfpath, verbose=verbose)
        del filelist
    else:
        # process multiple file with multi-threading:
        from multiprocessing import Pool
        from functools import partial
        if nFiles < ncores:
            pool = Pool(nFiles)
        else:
            pool = Pool(ncores)

        func = partial(batch_processor, filelist=filelist, outdir=outdir,
                       xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax,
                       diameter=diameter, vector=vector, csv=csv, sep=sep,
                       header=header, index=index, hdf=hdf, hdfpath=hdfpath,
                       verbose=verbose)

        pool.map(func, range(0, nFiles))
        pool.close()
        pool.join()
        del pool, func, filelist
        print('All Files Processed.')
